package gowam

const (
	MAX_OP_COUNT = 50000000
)

type Machine struct {
	debug bool
	benchmark bool
	maxOpCount int
	opCount int
	backtrackCount int
	program *Program
	trail *Trail
	failed bool
	displayQValue map[int]bool
	displayQCount int
	queryVariables map[int]string
	programCounter int
	continuationPointer int
	choicePoint *ChoicePoint
	cutPoint *ChoicePoint
	env *Environment
	arguments []*Variable
}

func NewMachine(program **Program) *Machine {
	
	m := &Machine{
		program: nil,
		debug: true,
		benchmark: true,
		maxOpCount: MAX_OP_COUNT,
		opCount: 0,
		backtrackCount: 0,
		trail: nil,
		failed: false,
		displayQValue: make(map[int]bool),
		displayQCount: 0,
		queryVariables: make(map[int]string),
		programCounter: 0,
		continuationPointer: 0,
		choicePoint: nil,
		cutPoint: nil,
		env: nil,
		arguments: make([]*Variable, 0),
	}

	program.SetOwner(m)

	m.SetProgram(program)

	m.Reset()

	return m
}

/**
 * resets sets all WAM parameters to their initial values
 */
func (m *Machine) Reset() {
    m.arguments = make([]*Variable, 0)  // no argument registers so far
    m.arguments = NewVariable()
    m.env = NewEnvironment(999999999, nil)  // empty environment
    m.continuationPointer = -1;  // no continuation point
    m.trail = new Trail($this);   // for undoing assert (see below)
    m.queryVariables = array();
    m.displayQCount = 0;
    m.displayQValue = array_fill(0, 100, false);
    m.choicePoint = null;
    m.cutPoint = null;
}
